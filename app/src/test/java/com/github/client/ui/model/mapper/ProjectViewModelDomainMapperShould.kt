package com.github.client.ui.model.mapper

import com.github.client.domain.model.Owner
import com.github.client.domain.model.Project
import org.junit.Assert.assertEquals
import org.junit.Test

class ProjectViewModelDomainMapperShould {

    private companion object {
        const val MOCK_PROJECT_ID = 123L
        const val MOCK_PROJECT_NAME = "any_name"
        const val MOCK_PROJECT_DESCRIPTION = "any_description"
        const val MOCK_PROJECT_UPDATED_AT = "2010-09-06T21:39:43Z"
        const val MOCK_PROJECT_STARGAZERS_COUNT = 12
        const val MOCK_PROJECT_WATCHERS_COUNT = 13
        const val MOCK_PROJECT_FORKS_COUNT = 14
        const val MOCK_PROJECT_ISSUES_COUNT = 15
        const val MOCK_OWNER_ID = 123
        const val MOCK_OWNER_LOGIN = "any_owner_login"
        const val MOCK_OWNER_AVATAR_URL = "any_owner_avatar_url"
        const val EXPECTED_VIEW_DATE = "21:39 - 06/09/2010"
    }

    @Test
    fun `map ProjectApiModel to Project domain model correctly`() {
        val project = buildProjectDomainModel()

        val mappedProjectViewModel = mapFromDomain(project)

        with(mappedProjectViewModel) {
            assertEquals(MOCK_PROJECT_ID, id)
            assertEquals(MOCK_PROJECT_NAME, name)
            assertEquals(MOCK_PROJECT_DESCRIPTION, description)
            assertEquals(EXPECTED_VIEW_DATE, updatedAt)
            assertEquals(MOCK_PROJECT_STARGAZERS_COUNT, stargazersCount)
            assertEquals(MOCK_PROJECT_WATCHERS_COUNT, watchersCount)
            assertEquals(MOCK_PROJECT_FORKS_COUNT, forksCount)
            assertEquals(MOCK_PROJECT_ISSUES_COUNT, openIssuesCount)
        }
    }

    private fun buildProjectDomainModel(): Project {
        val owner = Owner(MOCK_OWNER_ID, MOCK_OWNER_LOGIN, MOCK_OWNER_AVATAR_URL)
        return Project(
            MOCK_PROJECT_ID,
            MOCK_PROJECT_NAME,
            MOCK_PROJECT_DESCRIPTION,
            owner,
            MOCK_PROJECT_UPDATED_AT,
            MOCK_PROJECT_STARGAZERS_COUNT,
            MOCK_PROJECT_WATCHERS_COUNT,
            MOCK_PROJECT_FORKS_COUNT,
            MOCK_PROJECT_ISSUES_COUNT
        )
    }

}