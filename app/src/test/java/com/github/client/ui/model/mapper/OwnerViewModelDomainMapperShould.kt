package com.github.client.ui.model.mapper

import com.github.client.domain.model.Owner
import org.junit.Assert.assertEquals
import org.junit.Test

class OwnerViewModelDomainMapperShould {

    private companion object {
        const val MOCK_OWNER_ID = 123
        const val MOCK_OWNER_LOGIN = "any_owner_login"
        const val MOCK_OWNER_AVATAR_URL = "any_owner_avatar_url"
    }

    @Test
    fun `correctly map owner domain model to owner view model`() {
        val owner = Owner(MOCK_OWNER_ID, MOCK_OWNER_LOGIN, MOCK_OWNER_AVATAR_URL)

        val mappedOwnerViewModel = mapFromDomain(owner)

        with(mappedOwnerViewModel) {
            assertEquals(MOCK_OWNER_ID, id)
            assertEquals(MOCK_OWNER_LOGIN, login)
            assertEquals(MOCK_OWNER_AVATAR_URL, avatarUrl)
        }
    }
}