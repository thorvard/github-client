package com.github.client.domain.usecase

import arrow.core.Option
import arrow.core.Try
import com.github.client.domain.model.Project
import com.github.client.domain.repository.ProjectRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class SearchTrendingAndroidProjectsUseCaseShould {

    private lateinit var searchTrendingAndroidProjectsUseCase: SearchTrendingAndroidProjectsUseCase
    @Mock lateinit var projectRepository: ProjectRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        searchTrendingAndroidProjectsUseCase = SearchTrendingAndroidProjectsUseCase(projectRepository)
    }

    @Test
    fun `load projects from local when stored previously`() {
        whenever(projectRepository.loadProjects()).thenReturn(Option.just(arrayListOf()))

        searchTrendingAndroidProjectsUseCase.execute()

        verify(projectRepository, never()).fetchTrendingProjects(any(), any())
    }

    @Test
    fun `fetch projects from cloud when not stored previously`() {
        val fetchedProjects: List<Project> = arrayListOf()
        whenever(projectRepository.loadProjects()).thenReturn(Option.empty())
        whenever(projectRepository.fetchTrendingProjects(any(), any())).thenReturn(Try.Success(fetchedProjects))
        whenever(projectRepository.storeProjects(any())).thenReturn(Try.Success(Unit))

        searchTrendingAndroidProjectsUseCase.execute()

        verify(projectRepository).fetchTrendingProjects(any(), any())
    }

    @Test
    fun `store fetched projects in local when local storage is empty and cloud request succeeded`() {
        val fetchedProjects: List<Project> = arrayListOf()
        whenever(projectRepository.loadProjects()).thenReturn(Option.empty())
        whenever(projectRepository.fetchTrendingProjects(any(), any())).thenReturn(Try.Success(fetchedProjects))
        whenever(projectRepository.storeProjects(fetchedProjects)).thenReturn(Try.Success(Unit))

        searchTrendingAndroidProjectsUseCase.execute()

        verify(projectRepository).storeProjects(fetchedProjects)
    }

    @Test
    fun `not store fetched projects in local when local storage is empty and cloud request fails`() {
        whenever(projectRepository.loadProjects()).thenReturn(Option.empty())
        whenever(projectRepository.fetchTrendingProjects(any(), any())).thenReturn(Try.Failure(Exception()))

        searchTrendingAndroidProjectsUseCase.execute()

        verify(projectRepository, never()).storeProjects(any())
    }
}