package com.github.client.data.api.model.mapper

import com.github.client.data.api.model.OwnerApiModel
import org.junit.Assert.assertEquals
import org.junit.Test

class OwnerApiModelDomainMapperShould() {

    private companion object {
        const val MOCK_OWNER_ID = 123
        const val MOCK_OWNER_LOGIN = "any_owner_login"
        const val MOCK_OWNER_AVATAR_URL = "any_owner_avatar_url"
    }

    @Test
    fun `map owner api model to owner domain model with correct attributes`() {
        val ownerApiModel = OwnerApiModel(MOCK_OWNER_ID, MOCK_OWNER_LOGIN, MOCK_OWNER_AVATAR_URL)

        val mappedOwnerDomainModel = mapToDomain(ownerApiModel)

        with(mappedOwnerDomainModel) {
            assertEquals(MOCK_OWNER_ID, id)
            assertEquals(MOCK_OWNER_LOGIN, login)
            assertEquals(MOCK_OWNER_AVATAR_URL, avatarUrl)
        }
    }
}