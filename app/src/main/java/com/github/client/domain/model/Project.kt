package com.github.client.domain.model

data class Project(
    val id: Long,
    val name: String,
    val description: String,
    val owner: Owner,
    val updatedAt: String,
    val stargazersCount: Int,
    val watchersCount: Int,
    val forksCount: Int,
    val openIssuesCount: Int
)