package com.github.client.domain.model

data class Owner(
    val id: Int,
    val login: String,
    val avatarUrl: String
)