package com.github.client.domain.usecase

import arrow.core.Option
import com.github.client.domain.model.Project
import com.github.client.domain.repository.ProjectRepository

class GetProjectByIdUseCase(private val repository: ProjectRepository) {
    fun execute(projectId: Long): Option<Project> {
        return repository.loadProjectById(projectId)
    }
}