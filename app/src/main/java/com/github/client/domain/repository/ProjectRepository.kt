package com.github.client.domain.repository

import arrow.core.Option
import arrow.core.Try
import com.github.client.data.api.datasource.ProjectCloudDataSource
import com.github.client.data.local.ProjectLocalDataSource
import com.github.client.domain.model.Project

class ProjectRepository(
    private val projectCloudDataSource: ProjectCloudDataSource,
    private val projectLocalDataSource: ProjectLocalDataSource
) {

    fun fetchTrendingProjects(query: String, pageSize: Int): Try<List<Project>> {
        return projectCloudDataSource.getTrendingProjectsByQuery(query, pageSize)
    }

    fun loadProjects(): Option<List<Project>> {
        return projectLocalDataSource.getProjects()
    }

    fun storeProjects(projects: List<Project>): Try<Unit> {
        return projectLocalDataSource.storeProjects(projects)
    }

    fun loadProjectById(projectId: Long): Option<Project> {
        return projectLocalDataSource.getProjectById(projectId)
    }
}