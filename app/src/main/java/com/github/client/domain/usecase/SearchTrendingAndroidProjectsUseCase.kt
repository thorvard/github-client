package com.github.client.domain.usecase

import arrow.core.Try
import com.github.client.domain.model.Project
import com.github.client.domain.repository.ProjectRepository

class SearchTrendingAndroidProjectsUseCase(private val projectRepository: ProjectRepository) {

    private companion object {
        const val QUERY_STRING = "android"
        const val PAGE_SIZE = 20
    }

    fun execute(): Try<List<Project>> {
        return projectRepository.loadProjects()
            .fold(
                {
                    fetchProjects().flatMap { downloadedProjects ->
                        storeProjects(downloadedProjects)
                    }
                },
                { Try.just(it) }
            )
    }

    private fun fetchProjects(): Try<List<Project>> {
        return projectRepository.fetchTrendingProjects(QUERY_STRING, PAGE_SIZE)
    }

    private fun storeProjects(projects: List<Project>): Try<List<Project>> {
        return projectRepository.storeProjects(projects)
            .map { projects }
    }
}