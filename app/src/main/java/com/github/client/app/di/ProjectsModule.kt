package com.github.client.app.di

import com.github.client.data.api.datasource.ProjectCloudDataSource
import com.github.client.data.local.ProjectLocalDataSource
import com.github.client.domain.repository.ProjectRepository
import com.github.client.domain.usecase.GetProjectByIdUseCase
import com.github.client.domain.usecase.SearchTrendingAndroidProjectsUseCase
import com.github.client.ui.project.detail.ProjectDetailPresenter
import com.github.client.ui.project.list.ProjectsListPresenter
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

val projectsModule: Kodein.Module = Kodein.Module("projectsModule") {
    bind<ProjectCloudDataSource>() with provider {
        ProjectCloudDataSource(instance(), instance())
    }

    bind<ProjectLocalDataSource>() with singleton {
        ProjectLocalDataSource()
    }

    bind<ProjectRepository>() with provider {
        ProjectRepository(instance(), instance())
    }
}

val projectListModule: Kodein.Module = Kodein.Module("projectListModule") {
    bind<SearchTrendingAndroidProjectsUseCase>() with provider {
        SearchTrendingAndroidProjectsUseCase(instance())
    }

    bind<ProjectsListPresenter>() with provider {
        ProjectsListPresenter(instance())
    }
}

val projectDetailModule: Kodein.Module = Kodein.Module("projectDetailModule") {
    bind<GetProjectByIdUseCase>() with provider {
        GetProjectByIdUseCase(instance())
    }

    bind<ProjectDetailPresenter>() with provider {
        ProjectDetailPresenter(instance())
    }
}