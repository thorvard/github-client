package com.github.client.app.di

import com.github.client.BuildConfig
import com.github.client.data.api.retrofit.call.RetrofitCallFactory
import com.github.client.data.api.retrofit.converter.GithubSearchApiConverterFactory
import com.github.client.data.api.retrofit.service.GitHubService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val SERVER_URL = "server_url"
private const val GSON_CONVERTER_FACTORY = "gson_converter_factory"
private const val GITHUB_CONVERTER_FACTORY = "custom_converter_factory"

val restApiModule = Kodein.Module("rest_api_module") {
    bind<String>(tag = SERVER_URL) with singleton { "https://api.github.com/search/" }
}

val retrofitModule = Kodein.Module("retrofit_module") {
    bind<List<Interceptor>>() with singleton {
        val interceptorsList = mutableListOf<Interceptor>()
        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            interceptorsList.add(httpLoggingInterceptor)
        }
        interceptorsList
    }

    bind<OkHttpClient>() with singleton {
        val interceptors: List<Interceptor> = instance()
        val builder: OkHttpClient.Builder = OkHttpClient.Builder()
        interceptors.forEach { builder.addInterceptor(it) }
        builder.build()
    }

    bind<Converter.Factory>(tag = GSON_CONVERTER_FACTORY) with singleton {
        GsonConverterFactory.create()
    }

    bind<Converter.Factory>(tag = GITHUB_CONVERTER_FACTORY) with singleton {
        val gsonConverterFactory = instance<Converter.Factory>(GSON_CONVERTER_FACTORY) as GsonConverterFactory
        GithubSearchApiConverterFactory(gsonConverterFactory)
    }

    bind<Retrofit>() with singleton {
        val client = instance<OkHttpClient>()
        val baseURL = instance<String>(SERVER_URL)
        val githubSearchApiConverterFactory = instance<Converter.Factory>(GITHUB_CONVERTER_FACTORY)
        val gsonConverterFactory = instance<Converter.Factory>(GSON_CONVERTER_FACTORY)
        Retrofit.Builder()
                .baseUrl(baseURL)
                .client(client)
                .addConverterFactory(githubSearchApiConverterFactory)
                .addConverterFactory(gsonConverterFactory)
                .build()
    }

    bind<RetrofitCallFactory>() with singleton {
        RetrofitCallFactory()
    }

    bind<GitHubService>() with singleton {
        val retrofit: Retrofit = instance()
        retrofit.create(GitHubService::class.java)
    }
}