package com.github.client.app.di

import org.kodein.di.Kodein

val applicationComponent = Kodein {
    import(applicationModule)
    import(restApiModule)
    import(retrofitModule)
}

val projectsComponent = Kodein {
    extend(applicationComponent)
    import(projectsModule)
}

val projectsListComponent = Kodein {
    extend(projectsComponent)
    import(projectListModule)
}

val projectDetailComponent = Kodein {
    extend(projectsComponent)
    import(projectDetailModule)
}