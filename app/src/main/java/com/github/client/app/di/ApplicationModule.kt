package com.github.client.app.di

import org.kodein.di.Kodein

val applicationModule: Kodein.Module = Kodein.Module("application_module") {}