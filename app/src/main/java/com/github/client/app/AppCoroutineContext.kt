package com.github.client.app

import kotlin.coroutines.CoroutineContext

object AppCoroutineContext {
    lateinit var mainContext: CoroutineContext
    lateinit var backgroundContext: CoroutineContext
}