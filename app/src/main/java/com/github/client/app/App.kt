package com.github.client.app

import android.app.Application
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newFixedThreadPoolContext

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        setUpCoroutines()
    }

    private fun setUpCoroutines() {
        AppCoroutineContext.mainContext = Dispatchers.Main
        AppCoroutineContext.backgroundContext = newFixedThreadPoolContext(
            2 * Runtime.getRuntime().availableProcessors(),
            "bg"
        )
    }
}