package com.github.client.data.api.model

import com.google.gson.annotations.SerializedName

data class OwnerApiModel(
    val id: Int,
    val login: String,
    @SerializedName("avatar_url")
    val avatarUrl: String
)