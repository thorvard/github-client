package com.github.client.data.api.retrofit.call

import retrofit2.Call

interface RetrofitCall<T> {
    fun execute(call: Call<T>): T
}