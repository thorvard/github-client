package com.github.client.data.api.datasource

import arrow.core.Try
import com.github.client.data.api.model.ProjectApiModel
import com.github.client.data.api.model.mapper.mapToDomain
import com.github.client.data.api.retrofit.call.RetrofitCallFactory
import com.github.client.data.api.retrofit.service.GitHubService
import com.github.client.domain.model.Project

class ProjectCloudDataSource(
    private val callFactory: RetrofitCallFactory,
    private val gitHubService: GitHubService
) {

    fun getTrendingProjectsByQuery(query: String, pageSize: Int): Try<List<Project>> {
        return Try.invoke {
            val call = gitHubService.searchTrendingRepositories(query, pageSize)
            callFactory.getSyncCall<List<ProjectApiModel>>().execute(call)
                .map { mapToDomain(it) }
        }
    }
}