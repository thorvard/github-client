package com.github.client.data.local

import arrow.core.Option
import arrow.core.Try
import com.github.client.domain.model.Project

class ProjectLocalDataSource {
    private val projects: MutableMap<Long, Project> by lazy {
        mutableMapOf<Long, Project>()
    }

    fun storeProjects(projects: List<Project>): Try<Unit> {
        return Try.invoke { projects.forEach { this.projects[it.id] = it } }
    }

    fun getProjects(): Option<List<Project>> {
        return if (projects.isEmpty()) Option.empty() else Option.just(projects.values.toList())
    }

    fun getProjectById(projectId: Long): Option<Project> {
        return Option.fromNullable(projects[projectId])
    }
}