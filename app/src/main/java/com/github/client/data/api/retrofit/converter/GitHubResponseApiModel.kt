package com.github.client.data.api.retrofit.converter

import com.google.gson.annotations.SerializedName

data class GitHubResponseApiModel<T>(
    @SerializedName("total_count") val totalCount: Long,
    @SerializedName("incomplete_results") val incompleteResults: Boolean,
    val items: T
)