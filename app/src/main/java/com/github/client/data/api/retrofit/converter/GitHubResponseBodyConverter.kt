package com.github.client.data.api.retrofit.converter

import okhttp3.ResponseBody
import retrofit2.Converter
import java.io.IOException

class GitHubResponseBodyConverter<T>(private val converter: Converter<ResponseBody, GitHubResponseApiModel<T>>) :
        Converter<ResponseBody, T> {
    @Throws(IOException::class)
    override fun convert(responseBody: ResponseBody): T {
        val response = converter.convert(responseBody)
        return response.items
    }
}