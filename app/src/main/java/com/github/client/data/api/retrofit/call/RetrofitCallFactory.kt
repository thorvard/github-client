package com.github.client.data.api.retrofit.call

class RetrofitCallFactory {
    fun <T> getSyncCall(): RetrofitCall<T> {
        return RetrofitSyncCall()
    }
}