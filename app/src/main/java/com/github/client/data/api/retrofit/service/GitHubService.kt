package com.github.client.data.api.retrofit.service

import com.github.client.data.api.model.ProjectApiModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GitHubService {
    @GET("repositories")
    fun searchTrendingRepositories(
        @Query("q") query: String,
        @Query("per_page") pageSize: Int,
        @Query("sort") sort: String = "stars"
    ): Call<List<ProjectApiModel>>
}