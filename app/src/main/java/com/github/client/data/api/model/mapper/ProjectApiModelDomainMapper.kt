package com.github.client.data.api.model.mapper

import com.github.client.data.api.model.ProjectApiModel
import com.github.client.domain.model.Project

fun mapToDomain(source: ProjectApiModel): Project {
    return Project(
        source.id,
        source.name,
        source.description,
        mapToDomain(source.owner),
        source.updatedAt,
        source.stargazersCount,
        source.watchersCount,
        source.forksCount,
        source.openIssuesCount
    )
}