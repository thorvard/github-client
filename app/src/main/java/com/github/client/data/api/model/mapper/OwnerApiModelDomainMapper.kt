package com.github.client.data.api.model.mapper

import com.github.client.data.api.model.OwnerApiModel
import com.github.client.domain.model.Owner

fun mapToDomain(source: OwnerApiModel): Owner {
    return Owner(
        source.id,
        source.login,
        source.avatarUrl
    )
}