package com.github.client.data.api.retrofit.converter

import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class GithubSearchApiConverterFactory(private val gsonConverterFactory: GsonConverterFactory) : Converter.Factory() {
    override fun responseBodyConverter(
            type: Type,
            annotations: Array<Annotation>,
            retrofit: Retrofit
    ): Converter<ResponseBody, *>? {
        val wrappedType = object : ParameterizedType {
            override fun getActualTypeArguments(): Array<Type> = arrayOf(type)
            override fun getOwnerType(): Type? = null
            override fun getRawType(): Type = GitHubResponseApiModel::class.java
        }
        val gsonConverter: Converter<ResponseBody, *>? =
                gsonConverterFactory.responseBodyConverter(wrappedType, annotations, retrofit)
        return GitHubResponseBodyConverter(gsonConverter as Converter<ResponseBody, GitHubResponseApiModel<Any>>)
    }
}