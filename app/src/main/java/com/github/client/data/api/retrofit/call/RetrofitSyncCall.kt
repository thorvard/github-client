package com.github.client.data.api.retrofit.call

import retrofit2.Call
import java.io.IOException

class RetrofitSyncCall<T> : RetrofitCall<T> {
    override fun execute(call: Call<T>): T {
        return try {
            call.execute().body() ?: throw Exception()
        } catch (e: IOException) {
            throw e
        }
    }
}