package com.github.client.data.api.model

import com.google.gson.annotations.SerializedName

data class ProjectApiModel(
    val id: Long,
    val name: String,
    val description: String,
    val owner: OwnerApiModel,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("stargazers_count")
    val stargazersCount: Int,
    @SerializedName("watchers_count")
    val watchersCount: Int,
    @SerializedName("forks_count")
    val forksCount: Int,
    @SerializedName("open_issues_count")
    val openIssuesCount: Int
)