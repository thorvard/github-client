package com.github.client.ui.project.list

import com.github.client.app.AppCoroutineContext
import com.github.client.domain.usecase.SearchTrendingAndroidProjectsUseCase
import com.github.client.ui.model.ProjectViewModel
import com.github.client.ui.model.mapper.mapFromDomain
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProjectsListPresenter(private val searchTrendingAndroidProjectsUseCase: SearchTrendingAndroidProjectsUseCase) {

    var view: View? = null

    fun onViewReady() {
        GlobalScope.launch(AppCoroutineContext.mainContext) {
            withContext(AppCoroutineContext.backgroundContext) {
                searchTrendingAndroidProjectsUseCase.execute()
            }.fold(
                { view?.renderProjectLoadError() },
                { projects -> view?.renderProjects(projects.map { mapFromDomain(it) }) }
            )
        }
    }

    interface View {
        fun renderProjects(projects: List<ProjectViewModel>)
        fun renderProjectLoadError()
    }
}