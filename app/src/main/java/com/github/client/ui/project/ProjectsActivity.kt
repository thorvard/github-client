package com.github.client.ui.project

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.github.client.R
import com.github.client.ui.project.detail.ProjectDetailFragment
import com.github.client.ui.project.list.ProjectsListFragment
import com.github.client.ui.utils.inflateContent
import com.github.client.ui.utils.replaceContent

class ProjectsActivity : AppCompatActivity(), ProjectsListFragment.Listener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_projects)
        if (savedInstanceState == null) {
            inflateProjectsList()
        }
    }

    override fun onProjectSelected(itemId: Long) {
        inflateProjectDetails(itemId)
    }

    private fun inflateProjectsList() {
        supportFragmentManager.inflateContent(R.id.content, ProjectsListFragment::class.java.name) {
            ProjectsListFragment.newInstance()
        }
    }

    private fun inflateProjectDetails(projectId: Long) {
        supportFragmentManager.replaceContent(R.id.content, ProjectDetailFragment::class.java.name) {
            ProjectDetailFragment.newInstance(projectId)
        }
    }
}