package com.github.client.ui.project.detail

import com.github.client.app.AppCoroutineContext
import com.github.client.domain.usecase.GetProjectByIdUseCase
import com.github.client.ui.model.ProjectViewModel
import com.github.client.ui.model.mapper.mapFromDomain
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProjectDetailPresenter(private val getProjectByIdUseCase: GetProjectByIdUseCase) {

    var view: View? = null

    fun onViewReady() {
        view?.let { view ->
            GlobalScope.launch(AppCoroutineContext.mainContext) {
                withContext(AppCoroutineContext.backgroundContext) {
                    getProjectByIdUseCase.execute(view.projectId)
                }.fold(
                    { view.renderProjectNotFoundError() },
                    { project -> view.renderProject(mapFromDomain(project)) }
                )
            }
        }
    }

    interface View {
        val projectId: Long
        fun renderProject(project: ProjectViewModel)
        fun renderProjectNotFoundError()
    }
}