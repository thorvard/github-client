package com.github.client.ui.model.mapper

import com.github.client.domain.model.Project
import com.github.client.ui.model.ProjectViewModel
import java.text.SimpleDateFormat
import java.util.Locale

fun mapFromDomain(source: Project): ProjectViewModel {
    return ProjectViewModel(
        source.id,
        source.name,
        source.description,
        mapFromDomain(source.owner),
        parseDate(source.updatedAt),
        source.stargazersCount,
        source.watchersCount,
        source.forksCount,
        source.openIssuesCount
    )
}

private const val DOMAIN_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"
private const val VIEW_DATE_FORMAT = "HH:mm - dd/MM/yyyy"

private fun parseDate(domainDate: String): String {
    val domainSimpleDateFormat = SimpleDateFormat(DOMAIN_DATE_FORMAT, Locale.getDefault())
    val viewSimpleDateFormat = SimpleDateFormat(VIEW_DATE_FORMAT, Locale.getDefault())
    val date = domainSimpleDateFormat.parse(domainDate)
    return viewSimpleDateFormat.format(date)
}