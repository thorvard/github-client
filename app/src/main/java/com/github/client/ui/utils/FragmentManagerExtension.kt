package com.github.client.ui.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

fun FragmentManager.inflateContent(containerViewId: Int, fragmentTag: String, fragmentBuilder: () -> Fragment) {
    var fragment = findFragmentByTag(fragmentTag)
    if (fragment == null) {
        fragment = fragmentBuilder()
    }
    beginTransaction()
        .replace(containerViewId, fragment, fragmentTag)
        .commit()
}

fun FragmentManager.replaceContent(containerViewId: Int, fragmentTag: String, fragmentBuilder: () -> Fragment) {
    var fragment = findFragmentByTag(fragmentTag)
    if (fragment == null) {
        fragment = fragmentBuilder()
    }
    beginTransaction()
        .replace(containerViewId, fragment, fragmentTag)
        .addToBackStack(fragmentTag)
        .commit()
}