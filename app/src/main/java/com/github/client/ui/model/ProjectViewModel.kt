package com.github.client.ui.model

data class ProjectViewModel(
    val id: Long,
    val name: String,
    val description: String,
    val owner: OwnerViewModel,
    val updatedAt: String,
    val stargazersCount: Int,
    val watchersCount: Int,
    val forksCount: Int,
    val openIssuesCount: Int
)