package com.github.client.ui.model

data class OwnerViewModel(
    val id: Int,
    val login: String,
    val avatarUrl: String
)