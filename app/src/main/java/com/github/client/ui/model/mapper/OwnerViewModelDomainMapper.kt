package com.github.client.ui.model.mapper

import com.github.client.domain.model.Owner
import com.github.client.ui.model.OwnerViewModel

fun mapFromDomain(source: Owner): OwnerViewModel {
    return OwnerViewModel(
        source.id,
        source.login,
        source.avatarUrl
    )
}