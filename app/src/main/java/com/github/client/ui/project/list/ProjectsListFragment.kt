package com.github.client.ui.project.list

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.client.R
import com.github.client.app.di.projectsListComponent
import com.github.client.ui.model.ProjectViewModel
import com.github.client.ui.project.list.adapter.ProjectsAdapter
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_projects_list.*
import org.kodein.di.generic.instance

class ProjectsListFragment : Fragment(), ProjectsListPresenter.View {
    companion object {
        private const val DEFAULT_SCROLL_POSITION = 0
        private const val BUNDLE_KEY_SCROLL_POSITION = "scroll_position"
        fun newInstance(): ProjectsListFragment = ProjectsListFragment()
    }

    private val adapter: ProjectsAdapter = ProjectsAdapter()
    private val presenter: ProjectsListPresenter by projectsListComponent.instance()
    private var scrollPosition: Int = DEFAULT_SCROLL_POSITION
    private var listener: Listener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_projects_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.view = this
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initAdapter()
        if (savedInstanceState != null) {
            scrollPosition = savedInstanceState.getInt(BUNDLE_KEY_SCROLL_POSITION, DEFAULT_SCROLL_POSITION)
        }
        presenter.onViewReady()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(BUNDLE_KEY_SCROLL_POSITION, getScrollPosition())
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = (activity as Listener)
    }

    override fun renderProjects(projects: List<ProjectViewModel>) {
        adapter.addProjects(projects)
        (projectsList.layoutManager as LinearLayoutManager).scrollToPosition(scrollPosition)
    }

    override fun renderProjectLoadError() {
        view?.let { view ->
            val errorSnackbar =
                Snackbar.make(view, getString(R.string.load_error), Snackbar.LENGTH_LONG)
            errorSnackbar.setAction(getString(R.string.retry)) {
                errorSnackbar.dismiss()
                presenter.onViewReady()
            }.show()
        }
    }

    private fun initAdapter() {
        val layoutManager = LinearLayoutManager(activity!!)
        projectsList.layoutManager = layoutManager
        projectsList.adapter = adapter
        adapter.itemClickListener = { listener?.onProjectSelected(it) }
        projectsList.addItemDecoration(DividerItemDecoration(projectsList.context, layoutManager.orientation))
        projectsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                scrollPosition = getScrollPosition()
            }
        })
    }

    private fun getScrollPosition(): Int {
        return projectsList?.let {
            (it.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
        } ?: scrollPosition
    }

    interface Listener {
        fun onProjectSelected(itemId: Long)
    }
}