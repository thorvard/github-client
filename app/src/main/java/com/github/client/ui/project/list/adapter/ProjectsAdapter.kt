package com.github.client.ui.project.list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.github.client.R
import com.github.client.ui.model.ProjectViewModel

class ProjectsAdapter : RecyclerView.Adapter<ProjectsAdapter.ViewHolder>() {
    private val projects: MutableList<ProjectViewModel> by lazy {
        mutableListOf<ProjectViewModel>()
    }
    var itemClickListener: ((itemId: Long) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProjectsAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_projects, parent, false)
        val viewHolder = ViewHolder(view)
        setClickListenerPerViewHolder(viewHolder)
        return viewHolder
    }

    override fun getItemCount(): Int = projects.size

    override fun onBindViewHolder(holder: ProjectsAdapter.ViewHolder, position: Int) {
        val project = projects[position]
        with(holder) {
            projectName.text = project.name
            projectDescription.text = project.description
            favouriteCount.text = project.stargazersCount.toString()
            lastUpdateDate.text = project.updatedAt
        }
    }

    fun addProjects(projects: List<ProjectViewModel>) {
        this.projects.addAll(projects)
        notifyDataSetChanged()
    }

    private fun setClickListenerPerViewHolder(viewHolder: ViewHolder) {
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val project = projects[position]
                itemClickListener?.invoke(project.id)
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val projectName: TextView = view.findViewById(R.id.projectName)
        val projectDescription: TextView = view.findViewById(R.id.projectDescription)
        val favouriteCount: TextView = view.findViewById(R.id.favouriteCount)
        val lastUpdateDate: TextView = view.findViewById(R.id.lastUpdateDate)
    }
}