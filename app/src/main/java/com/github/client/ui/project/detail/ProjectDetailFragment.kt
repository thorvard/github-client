package com.github.client.ui.project.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.client.R
import com.github.client.app.di.projectDetailComponent
import com.github.client.ui.model.ProjectViewModel
import com.github.client.ui.utils.loadFromUrl
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_project_detail.*
import org.kodein.di.generic.instance

class ProjectDetailFragment : Fragment(), ProjectDetailPresenter.View {

    companion object {
        private const val PROJECT_ID = "project_id"
        fun newInstance(projectId: Long): ProjectDetailFragment = ProjectDetailFragment().apply {
            val args = Bundle()
            args.putLong(PROJECT_ID, projectId)
            arguments = args
        }
    }

    override val projectId: Long by lazy {
        arguments!!.getLong(PROJECT_ID)
    }
    private val presenter: ProjectDetailPresenter by projectDetailComponent.instance()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_project_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.view = this
        presenter.onViewReady()
    }

    override fun renderProject(project: ProjectViewModel) {
        creatorAvatar.loadFromUrl(project.owner.avatarUrl)
        projectName.text = project.name
        projectDescription.text = project.description
        creatorName.text = project.owner.login
    }

    override fun renderProjectNotFoundError() {
        view?.let { view ->
            val errorSnackbar =
                Snackbar.make(view, getString(R.string.project_load_error), Snackbar.LENGTH_INDEFINITE)
            errorSnackbar.setAction(getString(R.string.retry)) {
                errorSnackbar.dismiss()
                presenter.onViewReady()
            }.show()
        }
    }

}