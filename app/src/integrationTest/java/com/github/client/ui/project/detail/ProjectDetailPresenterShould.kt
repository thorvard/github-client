package com.github.client.ui.project.detail

import arrow.core.Option
import com.github.client.IntegrationTest
import com.github.client.app.di.projectDetailComponent
import com.github.client.data.local.ProjectLocalDataSource
import com.github.client.domain.model.Owner
import com.github.client.domain.model.Project
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Test
import org.kodein.di.Copy
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class ProjectDetailPresenterShould : IntegrationTest() {
    private companion object {
        const val MOCK_PROJECT_ID = 123L
        const val MOCK_PROJECT_NAME = "any_name"
        const val MOCK_PROJECT_DESCRIPTION = "any_description"
        const val MOCK_PROJECT_UPDATED_AT = "2010-09-06T21:39:43Z"
        const val MOCK_PROJECT_STARGAZERS_COUNT = 12
        const val MOCK_PROJECT_WATCHERS_COUNT = 13
        const val MOCK_PROJECT_FORKS_COUNT = 14
        const val MOCK_PROJECT_ISSUES_COUNT = 15
        const val MOCK_OWNER_ID = 321
        const val MOCK_OWNER_LOGIN = "any_login"
        const val MOCK_OWNER_AVATAR_URL = "any_avatar_url"
    }

    private val testComponent = Kodein {
        extend(projectDetailComponent, copy = Copy.All)
        bind<ProjectLocalDataSource>(overrides = true) with singleton {
            mock<ProjectLocalDataSource>()
        }
    }

    private val presenter: ProjectDetailPresenter by testComponent.instance()
    @Mock
    lateinit var mockView: ProjectDetailPresenter.View
    private val projectLocalDataSource: ProjectLocalDataSource by testComponent.instance()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `render project information when loaded from local`() {
        val project: Project = buildProject()
        whenever(mockView.projectId).thenReturn(MOCK_PROJECT_ID)
        whenever(projectLocalDataSource.getProjectById(MOCK_PROJECT_ID)).thenReturn(Option.just(project))

        presenter.view = mockView
        presenter.onViewReady()

        verify(mockView).renderProject(any())
    }

    @Test
    fun `render error when project is not stored in local storage`() {
        whenever(projectLocalDataSource.getProjectById(any())).thenReturn(Option.empty())

        presenter.view = mockView
        presenter.onViewReady()

        verify(mockView).renderProjectNotFoundError()
    }

    private fun buildProject(): Project {
        return Project(
            MOCK_PROJECT_ID,
            MOCK_PROJECT_NAME,
            MOCK_PROJECT_DESCRIPTION,
            buildOwner(),
            MOCK_PROJECT_UPDATED_AT,
            MOCK_PROJECT_STARGAZERS_COUNT,
            MOCK_PROJECT_WATCHERS_COUNT,
            MOCK_PROJECT_FORKS_COUNT,
            MOCK_PROJECT_ISSUES_COUNT
        )
    }

    private fun buildOwner(): Owner {
        return Owner(
            MOCK_OWNER_ID,
            MOCK_OWNER_LOGIN,
            MOCK_OWNER_AVATAR_URL
        )
    }

}