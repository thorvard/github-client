package com.github.client.ui.project.list

import com.github.client.IntegrationTest
import com.github.client.app.di.projectsListComponent
import com.github.client.di.testRestModule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.verify
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Test
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class ProjectsListPresenterShould : IntegrationTest() {

    private val testComponent = Kodein {
        extend(testRestModule(projectsListComponent))
    }

    private companion object {
        const val PROJECTS_REQUEST: String = "projects_request.json"
    }

    private val mockWebServer: MockWebServer by testComponent.instance()
    private val presenter: ProjectsListPresenter by testComponent.instance()
    @Mock
    lateinit var mockView: ProjectsListPresenter.View

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `render projects when request success`() {
        mockWebServer.enqueue(
            MockResponse()
                .setBody(requestFromFile(PROJECTS_REQUEST))
                .setResponseCode(200)
        )

        presenter.view = mockView
        presenter.onViewReady()

        verify(mockView).renderProjects(any())
    }

    @Test
    fun `render error when request fails`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(500))

        presenter.view = mockView
        presenter.onViewReady()

        verify(mockView).renderProjectLoadError()
    }

}