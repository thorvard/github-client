package com.github.client.di

import com.github.client.app.di.SERVER_URL
import okhttp3.mockwebserver.MockWebServer
import org.kodein.di.Copy
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

fun testRestModule(baseComponent: Kodein) = Kodein {
    extend(baseComponent, copy = Copy.All)

    bind<MockWebServer>() with singleton {
        MockWebServer()
    }

    bind<String>(tag = SERVER_URL, overrides = true) with singleton {
        val mockWebServer: MockWebServer = instance()
        mockWebServer.url("/").toString()
    }
}