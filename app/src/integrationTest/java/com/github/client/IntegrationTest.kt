package com.github.client

import com.github.client.app.AppCoroutineContext
import kotlinx.coroutines.CoroutineDispatcher
import org.junit.BeforeClass
import kotlin.coroutines.CoroutineContext

abstract class IntegrationTest {
    object TestCoroutineDispatcher : CoroutineDispatcher() {
        override fun isDispatchNeeded(context: CoroutineContext): Boolean = false
        override fun dispatch(context: CoroutineContext, block: Runnable) {
            block.run()
        }

        override fun toString(): String = "Test"
    }

    companion object {
        @BeforeClass
        @JvmStatic
        fun setUpClass() {
            AppCoroutineContext.mainContext = TestCoroutineDispatcher
            AppCoroutineContext.backgroundContext = TestCoroutineDispatcher
        }
    }

    protected fun requestFromFile(path: String): String {
        val stream = javaClass.classLoader.getResourceAsStream(path)
        return stream.bufferedReader().use { it.readText() }
    }
}